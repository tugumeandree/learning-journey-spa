import firebase from "firebase/app";
import "firebase/auth";

// const   firebaseConfig = {
//     apiKey: process.env.REACT_APP_FIREBASE_KEY,
//     authDomain: process.env.REACT_APP_DOMAIN,
//     databaseURL: process.env.REACT_APP_DATABASE,
//     projectId: process.env.REACT_APP_PROJECT_ID,
//     storageBucket: process.env.REACT_APP_STORAGE_BUCKET,
//     messagingSenderId: process.env.REACT_APP_SENDER_ID,
//     appId: process.env.REACT_APP_APP_ID,
//     measurementId: process.env.REACT_APP_MEASUREMENT_ID,

//   };

  const firebaseConfig = {
    apiKey: "AIzaSyA9UUfAacEwZhPmE_33GnDaDwfyjhf8bJ0",
    authDomain: "starthub-ca6f3.firebaseapp.com",
    databaseURL: "https://starthub-ca6f3-default-rtdb.firebaseio.com",
    projectId: "starthub-ca6f3",
    storageBucket: "starthub-ca6f3.appspot.com",
    messagingSenderId: "230232796391",
    appId: "1:230232796391:web:0aa453b97fa218f06f4642",
    measurementId: "G-JRY41FK0XW"
  };

  const base = firebase.initializeApp(firebaseConfig);
  export default base;