import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import "./App.css";
import Homepage from "../src/components/auth/home";
import Login from "../src/components/auth/Login";
import SignUp from "../src/components/auth/Signup";
// import Registration from "../src/components/auth/Signup2";
import {AuthProvider} from "../src/components/context/Auth";
// import PrivateRoute from "../src/components/auth/privateRoute";

import Study from "./components/features/Study";
import Dash from "./components/features/Dash";
import Milestones from "./components/features/Milestones";
import Schedule from "./components/features/Schedule";
import Model from "./components/features/Model";
import Tasks from "./components/features/Tasks";




function App () {

  
    return (
      <AuthProvider>
        
        <Router>

        <Switch>
       
        <Route exact path="/" component= {Homepage}/>
        <Route  exact path="/login" component= {Login}/>
        <Route  exact path="/signup" component= {SignUp}/>
        {/* <Route  exact path="/signup" component= {Registration}/> */}
          <Route path="/study" component={Study}></Route>
          <Route path="/schedule" component={Schedule}></Route>
          <Route path="/model" component={Model}></Route>
          <Route path="/milestones" component={Milestones}></Route>
          <Route path="/dash" component={Dash}></Route>
          <Route path="/tasks" component={Tasks}></Route>
           
        </Switch>
        </Router>
        </AuthProvider>
    )
}



export default App;

