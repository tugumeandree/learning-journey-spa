import React from "react";
import ReactDOM from "react-dom";
import SideNav from "./SideNav";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import useWindowSize from "./hooks";

import "./styles.css";

const Home = () => <div>Home</div>;
const About = () => <div>About</div>;
const Stuff = () => {
  const size = useWindowSize();

  return (
    <div>
      {size.width}px / {size.height}px
    </div>
  );
};

function App() {
  const menuPaths = [
    {
      name: "Home",
      path: "/"
    },
    {
      name: "About",
      path: "/about"
    },
    {
      name: "Stuff",
      path: "/stuff"
    }
  ];
  return (
    <Router>
      <div className="App">
        <SideNav paths={menuPaths} title="Menu" />
        <div className="container-margin">
          <div className="container">
            <Route path="/" exact component={Home} />
            <Route path="/about/" component={About} />
            <Route path="/stuff/" component={Stuff} />
          </div>
        </div>
      </div>
    </Router>
  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
