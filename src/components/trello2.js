import React from "react";
import data from "./data.json";
import Board from "react-trello";

export default function Trello() {
    return (
        <div className="App">
        <h1>react-trello demo</h1>
        <Board data={data} draggable />
      </div>
    )
}
