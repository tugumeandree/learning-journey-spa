import React from 'react'

export default function Index() {
    return (
        <div className="center">
          <div className="navbar-fixed">
            <nav className="nav" role="navigation">
              <div className="nav-wrapper">
                <a href><img className="brand-logo" src="../img/logo.png" /></a>
                <a href="#" data-target="nav-mobile" className="sidenav-trigger"><i className="material-icons">menu</i></a>
                <ul className="left hide-on-med-and-down">
                  <li className="logged-out">
                    <a href="https://airtable.com/shrS6aSAZIgqjP1g0" target="_blank">Apply</a>
                  </li>
                  <li className="logged-out">
                    <a href="/pages/login.html">Login</a>
                  </li>
                  <li className="logged-out">
                    <a href="/pages/signUp.html">Sign up</a>
                  </li>
                  <li className="logged-in" style={{display: 'none'}}>
                    <a href="#" className="grey-text modal-trigger" data-target="modal-account">Account</a>
                  </li>
                  <li className="admin" style={{display: 'none'}}>
                    <a href="#" className="grey-text modal-trigger" data-target="modal-create">Assign Content</a>
                  </li>
                  <li className="logged-in" style={{display: 'none'}}>
                    <a href="#" className="grey-text" id="logout">Logout</a>
                  </li>
                </ul>
                <ul id="nav-mobile" className="sidenav">
                  <li className="logged-out">
                    <a href="https://airtable.com/shrS6aSAZIgqjP1g0" target="_blank">Apply</a>
                  </li>
                  <li className="logged-out">
                    <a href="/pages/login.html">Login</a>
                  </li>
                  <li className="logged-out">
                    <a href="/pages/signUp.html">Sign up</a>
                  </li>
                  <li className="logged-in" style={{display: 'none'}}>
                    <a href="#" className="grey-text modal-trigger" data-target="modal-account">Account</a>
                  </li>
                  <li className="admin" style={{display: 'none'}}>
                    <a href="#" className="grey-text modal-trigger" data-target="modal-create">Assign Content</a>
                  </li>
                  <li className="logged-in" style={{display: 'none'}}>
                    <a href="#" className="grey-text" id="logout">Logout</a>
                  </li>
                </ul>
              </div>
            </nav>
          </div>
          <div id="container" className="center container">
            <div className="row">
              {/* Cards container */}
              <div> 
                <div className="center">
                  <h1 className="center" />
                  <h4>The Starthub Catalyzer Online Lab</h4>
                  <h5>African Startup Accelerator In Uganda</h5>
                  <p className="flow-text center">1-on-1 guidance; from business modeling to investment &amp; continuous growth for 2 years.
                  </p>
                </div>
                <div id="card-container" className="row">
                  {/* Col: Card 1 */}
                  <div className="col s12 m6 l3">
                    {/* Card 1 */}
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <a href="#"><i className="small material-icons">room</i>Online Toolkit</a>
                          <time>17th March</time>
                        </div>
                        <span className="card-title grey-text text-darken-4">Learning</span>
                        <p className="card-subtitle grey-text text-darken-2">Interact With General And Customised Content You Need </p>
                        <p className="card-subtitle grey-text text-darken-2">Guided Learning Journey </p>
                      </div>
                      <div className="card-action">
                        <a href="pages/login.html"><i className="material-icons">&nbsp;language</i>LOGIN</a>
                      </div>
                    </div>
                    {/* End of card */}
                  </div>
                  {/* End of col */}
                  {/* Col: Card 1 */}
                  <div className="col s12 m6 l3">
                    {/* Card 1 */}
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <a href="#"><i className="small material-icons">room</i>Mentorship</a>
                          <time>17th March</time>
                        </div>
                        <span className="card-title grey-text text-darken-4">Data Driven Continous Growth</span>
                        <p className="card-subtitle grey-text text-darken-2">Our Coaches freely share their wisdom &amp; guidance creating real and lasting relationships like a co-founder</p>
                      </div>
                      <div className="card-action">
                        <a href="pages/signUp.html"><i className="material-icons">&nbsp;language</i>LOGIN</a>
                      </div>
                    </div>
                    {/* End of card */}
                  </div>
                  {/* Col: Card 1 */}
                  <div className="col s12 m6 l3">
                    {/* Card 1 */}
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <a href="#"><i className="small material-icons">room</i>Investors</a>
                          <time>17th March</time>
                        </div>
                        <span className="card-title grey-text text-darken-4">Investment</span>
                        <p className="card-subtitle grey-text text-darken-2">
                          Starthub connects investors with the most promising startups to create real change around the world.</p>
                        <p className="card-subtitle grey-text text-darken-2">4M UGX in cash to boost your Startup</p>
                      </div>
                      <div className="card-action">
                        <a href="https://www.node.codestudioug.com/"><i className="material-icons">&nbsp;language</i>LOGIN</a>
                      </div>
                    </div>
                    {/* End of card */}
                  </div>
                  <section>
                    <div className="container">
                      <div className="row">
                        <h3>Startups</h3>
                        <hr />
                        <div className="col s12 m6">
                          <div className="card horizontal">
                            <div className="card-image">
                              <img src="https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-9/30594921_486671951735105_7887264765414735872_n.jpg?_nc_cat=106&ccb=3&_nc_sid=09cbfe&_nc_ohc=hEA-ZuK4eOIAX_karEs&_nc_ht=scontent-frt3-1.xx&oh=fc7464875c16433b8fd34705b17420a6&oe=6053788A" />
                              <span className="card-badge orange">Legal</span>
                            </div>
                            <div className="card-stacked">
                              <div className="card-content">
                                <h4>Legal Care</h4>
                              </div>
                              <div className="card-action">
                                <i className="large material-icons">web</i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col s12 m6">
                          <div className="card horizontal">
                            <div className="card-image">
                              <img src="https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-9/130270038_102325878424568_6586310390738645900_o.jpg?_nc_cat=108&ccb=3&_nc_sid=09cbfe&_nc_ohc=71L2pIsNQIsAX-E8ofD&_nc_ht=scontent-frt3-1.xx&oh=07a1ef03e9c547ab0bcb8da4615c4853&oe=6054169B" />
                              <span className="card-badge orange">Sport</span>
                            </div>
                            <div className="card-stacked">
                              <div className="card-content">
                                <h4>OnscoreAfrica</h4>
                              </div>
                              <div className="card-action">
                                <i className="large material-icons">web</i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col s12 m6">
                          <div className="card horizontal">
                            <div className="card-image">
                              <img src="https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/106598838_176929487124726_3126507027295617012_o.jpg?_nc_cat=105&ccb=3&_nc_sid=09cbfe&_nc_ohc=Q3BacdzlV7gAX9ubUlv&_nc_ht=scontent-frx5-1.xx&oh=09eb3b7ea2189f36b05281ea1aa09ad2&oe=60531460" />
                              <span className="card-badge orange">Tech</span>
                            </div>
                            <div className="card-stacked">
                              <div className="card-content">
                                <h4>Isharc Inc.</h4>
                              </div>
                              <div className="card-action">
                                <i className="large material-icons">web</i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col s12 m6">
                          <div className="card horizontal">
                            <div className="card-image">
                              <img src="https://scontent-frt3-1.xx.fbcdn.net/v/t1.0-9/122749735_217405706388971_8949668547653505170_n.jpg?_nc_cat=107&ccb=3&_nc_sid=09cbfe&_nc_ohc=sWCKSNguQ6cAX9TNbk0&_nc_ht=scontent-frt3-1.xx&oh=45d171987b71753ba6159f19f4b00a2e&oe=6055034A" />
                              <span className="card-badge orange">Tech</span>
                            </div>
                            <div className="card-stacked">
                              <div className="card-content">
                                <h4>Inove-Labs</h4>
                              </div>
                              <div className="card-action">
                                <i className="large material-icons">web</i>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="col s12 m6">
                          <div className="card horizontal">
                            <div className="card-image">
                              <img src="https://i.pinimg.com/280x280_RS/9e/31/6d/9e316d197d4d3ceb343f4b4dd65284c2.jpg" />
                              <span className="card-badge orange">Fitness</span>
                            </div>
                            <div className="card-stacked">
                              <div className="card-content">
                                <h4>OMNI Gym</h4>
                              </div>
                              <div className="card-action">
                                <i className="large material-icons">web</i>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                </div>
              </div>
            </div>
          </div>
          <div className="container card">
            <div className="col s12 m12 l3">
              <div className="right">
                <span className="card__date__day"><img className="brand-logo" src="img/logo.png" /></span>
                {/* <span class="card__date__month">Sept</span> */}
              </div>
              <img src="https://assets.entrepreneur.com/content/3x2/2000/20200428200512-Ent-Illustrator.png" />
              <ul className="collapsible" data-collapsible="expandable">
                <li className>
                  <div className="collapsible-header active"><i className="material-icons">business</i><a href="pages/signUp.html">Startups</a></div>
                  <div className="collapsible-body filter-container" style={{display: 'none', paddingTop: '0px', marginTop: '0px', paddingBottom: '0px', marginBottom: '0px'}}>
                    <form action="#">
                      <ul>
                        <li>
                          <input className="with-gap" name="group1" type="radio" id="Today" />
                          <label htmlFor="Today">Today</label>
                        </li>
                        <li>
                          <input className="with-gap" name="group1" type="radio" id="week" />
                          <label htmlFor="week">This week</label>
                        </li>
                        <li>
                          <input className="with-gap" name="group1" type="radio" id="month" selected />
                          <label htmlFor="month">This month</label>
                        </li>
                        <li>
                          <input className="with-gap" name="group1" type="radio" id="year" />
                          <label htmlFor="year">This year</label>
                        </li>
                        <li>
                          <input className="with-gap" name="group1" type="radio" id="Custom" />
                          <label htmlFor="Custom">Custom date</label>
                        </li>
                      </ul>
                    </form>
                  </div>
                </li>
                <li className>
                  <div className="collapsible-header active"><i className="material-icons">business_center</i><a href="pages/signUp.html">Coaches</a></div>
                  <div className="collapsible-body " style={{display: 'none', paddingTop: '0px', marginTop: '0px', paddingBottom: '0px', marginBottom: '0px'}}>
                    <p>
                      <select multiple>
                        {/*<option value="" disabled selected>Choose your option</option>*/}
                        <option value={1} selected>Lindy Hop</option>
                        <option value={2} selected>Blues</option>
                        <option value={3} selected>Balboa</option>
                        <option value={3} selected>Jazz</option>
                        <option value={3} selected>Shag</option>
                        <option value={3} selected>Charleston</option>
                        <option value={3} selected>Other</option>
                      </select>
                      <label>Materialize Select</label>
                    </p>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header active"><i className="material-icons">contacts</i><a href="pages/signUp.html">Investors</a></div>
                  <div className="collapsible-body">
                    <p><select id="country-select" />
                      <label>Optgroups</label></p>
                  </div>
                </li>
                <li>
                  <div className="collapsible-header active"><i className="material-icons">class</i><a href="pages/signUp.html">Events</a></div>
                  <div className="collapsible-body filter-container">
                    <ul>
                      <li>
                        <input type="checkbox" name="event-type-filter" className="event-type-filter filled-in" id="socials" defaultChecked="checked" defaultValue="social" />
                        <label htmlFor="socials">Socials</label>
                      </li>
                      <li>
                        <input type="checkbox" name="event-type-filter" className="event-type-filter  filled-in" id="classes" defaultChecked="checked" defaultValue="class" />
                        <label htmlFor="classes">Classes</label>
                      </li>
                      <li>
                        <input type="checkbox" name="event-type-filter" className="event-type-filter filled-in" id="festival-event" defaultChecked="checked" defaultValue="festival" />
                        <label htmlFor="festival-event">Festivals &amp; events</label>
                      </li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <footer className="center">
            <p>All Rights Reserved</p>
            <p>Copyright © <a href="#"> www.starthub.africa </a>2021.</p>
          </footer>
        
          <div id="root" />
        </div>
      );
}
