// import { Trello } from "trello-helper";

// const trello = new Trello({ path: "../trello.env.json" });

// const demo = async () => {
//   // get all the cards on the specified list
//   const cardsOnList = await trello.getCardsOnList({
//     listId: "5c9a9d8afcf46f3f1fdb6698"
//   });
//   // get all the actions on card 123 that are of type 'moveToBoard'
//   const mtbActions = await trello.getActionsOnCard({
//     cardId: "5c9a9d95f770d24919eb7edb",
//     options: { filter: "moveToBoard" }
//   });
//   // get up to 1000 actions on a card
//   const actions = await trello.getActionsOnCard({
//     cardId: "5c9a9d95f770d24919eb7edb"
//   });
//   // get all the custom field data for a card
//   const cf = await trello.getCustomFieldItemsOnCard({
//     cardId: "5c9a9d95f770d24919eb7edb"
//   });
//   // set the value of a custom field
//   await trello.setCustomFieldValueOnCard({
//     cardFieldObj: {
//       cardId: "5c9a9d95f770d24919eb7edb",
//       fieldId: "5c9e45e513d1db64b50fdca2"
//     },
//     type: "text",
//     value: "some data"
//   });
// };