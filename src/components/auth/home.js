/* eslint-disable jsx-a11y/alt-text */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import base from  '../../base';
import {  BrowserRouter as Router,  Link } from "react-router-dom";



export default function Homepage() {
    return (
      
        <div>
          <Router>
          <link href="../css/main.css" type="text/css" rel="stylesheet" />
          <link rel="stylesheet" href="../css/styles.css" />

         
          <style dangerouslySetInnerHTML={{__html: "\n    body{\n      background-color: #F4F7F9;\n    }\n          .brand-logo{\n          width:45px;\n          }\n          \n          .nav {\n            background-color: #69191B !important;\n          }\n          \n          .btn-large {\n              background-color: #3D5728;\n              color: #DFA126;\n          }\n       \n          \n          " }} />
          <div className="wrapper">
            <div className="top_navbar">
              <div className="hamburger">
                <div className="one" />
                <div className="two" />
                <div className="three" />
              </div>
              <div className="top_menu">
                <div className="logo">   <span className="card__date__day"><img className="brand-logo" src="../img/logo.png"  alt="logo"/></span></div>
                <div className="left">
                  <h4>Starthub Catalyzer</h4>
                </div>
                <button onClick= {
                  
                  ()=>base.auth.signOut}>Sign Out</button>

                <form className="left">
                  <div className="input-field">
                    <input id="search" type="search" required />
                    <label className="label-icon" htmlFor="search"><i className="material-icons">search</i></label>
                    <i className="material-icons">close</i>
                  </div>
                </form>
                <ul>
   
                <li><a href="#">
                      <i className="fas fa-bell" />
                    </a></li>
                  <li><a href="#">
                      <i className="fas fa-user" />
                    </a></li>
                </ul>
              </div>
            </div>
            <div className="sidebar">
              <ul>
                <li><Link to="/" className="active">
                    <span className="icon"><i className="fas fa-book" /></span>
                    <span className="title">Services</span></Link> </li>
                <li><Link to="/dash">
                    <span className="icon"><i className="fas fa-leaf" /></span>
                    <span className="title">Dashboard</span>
                   </Link></li>
                <li><Link to="/schedule">
                    <span className="icon"><i className="fas fa-file-video" /></span>
                    <span className="title">Schedule</span></Link>
                  </li>
                <li><Link to="/study">
                    <span className="icon"><i className="fas fa-book" /></span>
                    <span className="title">Study</span></Link></li>
                <li><Link to="/model">
                    <span className="icon"><i className="fas fa-volleyball-ball" /></span>
                    <span className="title">Business Model</span>
                  </Link></li>
                <li><Link to="/milestones">
                    <span className="icon"><i className="fas fa-blog" /></span>
                    <span className="title">Milestone Tracker</span>
                  </Link></li>
                <li><Link to="/tasks">
                    <span className="icon"><i className="fas fa-leaf" /></span>
                    <span className="title">Todo List</span>
                  </Link></li>
              
              </ul>
            </div>
            <div className="main_container">
              <div className="container">
                <div className="row">
                  {/* Card 1 */}
                  <div className="col s12 m6 l4">
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/study">
                          <span className="card-title grey-text text-darken-4">Study</span>
                          <p className="card-subtitle grey-text text-darken-2">Interact With Customised Content You Need </p>
                          <i className="material-icons large">school</i> 
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/study"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                  {/* Card 1 */}
                  <div className="col s12 m6 l4">
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/schedule">
                          <span className="card-title grey-text text-darken-4">Schedule</span>
                          <p className="card-subtitle grey-text text-darken-2">Schedule Calls Or Book Meetings </p>
                          <i className="material-icons large">business</i> 
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/schedule"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                  {/* Card 1 */}
                  <div className="col s12 m6 l4">
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/model">
                          <span className="card-title grey-text text-darken-4">Business Model</span>
                          <p className="card-subtitle grey-text text-darken-2">Interate with a lean canvas </p>
                          <i className="material-icons large">business</i> 
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/model"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                  {/* Card 1 */}
                  <div className="col s12 m6 l4">
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/milestones">
                          <span className="card-title grey-text text-darken-4">Milestones</span>
                          <p className="card-subtitle grey-text text-darken-2">Archive Your Goals</p>
                          <i className="material-icons large">assignment</i> 
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/milestones"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                  {/* Card 1 */}
                  <div className="col s12 m6 l4"> 
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/dash">
                          <span className="card-title grey-text text-darken-4">Dashboard</span>
                          <p className="card-subtitle grey-text text-darken-2">Data Driven Growth </p>
                          <i className="material-icons large">event_note</i>
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/dash"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                  {/* Card 1 */}
                  <div className="col s12 m6 l4">
                    <div className="card">
                      <div className="card-content white-text">
                        <div className="right">
                          <span className="card__date__day"><img className="brand-logo" src="../img/logo.png" /></span>
                          {/* <span class="card__date__month">Sept</span> */}
                        </div>
                        <div className="card__meta">
                          <Link to="/"><i className="small material-icons">room</i>Online</Link>
                          <time>17th March</time>
                        </div>
                        <Link to="/tasks">
                          <span className="card-title grey-text text-darken-4">To Do's</span>
                          <p className="card-subtitle grey-text text-darken-2">Scheduled Tasks </p>
                          <i className="material-icons large">event_note</i>
                        </Link>
                      </div>
                      <div className="card-action">
                        <Link to="/tasks"><i className="material-icons">&nbsp;language</i>START</Link>
                      </div>
                    </div>
                  </div>
                  {/* End of card */}
                </div>
              </div>
            </div>
          </div>
          <footer className="center">
            <p>All Rights Reserved</p>
            <p>Copyright © <Link to="/"> www.starthub.africa </Link>2021.</p>
          </footer>
        
          {/* Compiled and minified JavaScript */}

          </Router>
        </div>
      );
}
