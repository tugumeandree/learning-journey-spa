import React , {useCallback, useContext} from "react";
import {withRouter,Redirect} from "react-router";
import base from "../../base";
import {AuthContext} from "../context/Auth";



const Login = ({history})=>{
    const handleLogin =   useCallback(
        async event => {
    event.preventdefault();
    const {email, password} = event.target.elements;
    try{
await base
.auth()
.signInWithEmailAndPassword(email.value,password.value);
history.push("/");
    } catch (error) {
alert(error);
    }
        },[history]
        
        );

        const {currentUser } = useContext (AuthContext);

        if (currentUser){
            return <Redirect to = "/login"/>
        }

        return (
            <div>

            <style dangerouslySetInnerHTML={{__html: "\n  div.progress {\n        display: none;\n    }\n\nhtml,\nbody,\n.login-box {\n  height: 100%;\n}\n@media only screen and (max-width: 992px) {\nheader,\nmain,\nfooter {\npadding-left: 0;\n}\n}\n\n.brand-logo{\nwidth:50px;\n}\n\n.head{\nwidth:100px;\n}\n\n.nav {\n  background-color: #69191B !important;\n}\n\n.btn-large {\n    background-color: #3D5728;\n    color: #DFA126;\n}\n\n.login-card{\n  margin: auto;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center; \n  width: 50%;\n  background-color: #fff;\n  box-sizing: border-box;\n  box-shadow: 1px 1px 5px rgba(0,0,0,0.5);\n  padding: 10px;\n}\n\n.login-input {\n  width: 80% !important;\n  margin-bottom: 20px !important;\n  background: rgb(228, 227, 227);\n}\n\n.login-btn{\n  width:50%;\n  height: 50px;\n  border: none;\n  background: #DFA126;\n  box-sizing: border-box;\n  box-shadow: 1px 1px 5px rgba(0,0,0,0.5);\n  border-radius: 5px;\n}\n\nimg {\n  width: 150px;\n  height: 150px ;\n}\n\n.span-div{\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n  margin-bottom: 20px;\n}\n\n\n\n.content {\n   margin:  auto;\n}\n\nbody{\n    background-color: #F4F7F9;\n  }\n\n  @media screen and (max-width: 768px) {\n    .login-card {\n                width: 100%;\n            }\n}\n\n" }} />
            <div className="left">
              <span className="card__date__day"><img className="head" src="../img/logo.png" /></span>
           
            </div>
            <div className="valign-wrapper row login-box">
              <div className="col card hoverable s10 pull-s1 m6 pull-m3 l4 pull-l4">
                <form onSubmit={handleLogin} id="login-form">
                  <div className="card-content">
                    <span className="card-title">
                      <p style={{textAlign: 'center'}}>Login to continue to: <br /> 
                        <strong style={{color: 'rgba(0,0,0,0.7)'}}>Starthub Africa Catalyzer</strong>
                      </p>
                    </span>
                    <div className="row">
                      <div className="input-field col s12">
                        <label htmlFor="login-email">Email address</label>
                        <input type="email" className="validate login-input" name="email" id="login-email" />
                      </div>
                      <div className="input-field col s12">
                        <label htmlFor="login-password">Password </label>
                        <input type="password" className="validate login-input" name="password" id="login-password" />
                      </div>
                    </div>
                  </div>
                  <div className="progress">
                    <div className="indeterminate" />
                  </div>
                  <div className="card-action center-align">
                   
                    <button type="submit" className="modal-trigger login-btn" data-target="modal-login" id="btnLogin">Login</button>
            
                    <button type="submit" className="modal-trigger login-btn hide" data-target="modal-login" id="btnLogout">Log Out</button>
                  </div>
                  <div className="card-action center-align">
                    <h6>Don't have account?</h6>
                    <a href="./signUp.html">Sign Up for an account</a>
                  </div>
                </form>
              </div>
            </div>
            <div className="right">
              <span className="card__date__day"><img className="head" src="../img/logo.png" /></span>
            
            </div>
           
          </div>
        )
}
  export default withRouter(Login);